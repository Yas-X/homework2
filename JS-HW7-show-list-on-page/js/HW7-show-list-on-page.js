const array = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

const cityList = document.querySelector('ul');

array.flat().map((city) => {
  cityList.innerHTML += `<li>${city}</li>`;
});

function printNumbers(from, to) {
  let current = from;
  let timer = document.createElement('div');

  let timerId = setInterval(function () {
    timer.innerHTML = (`<h2>${current}</h2>`);
    document.body.insertBefore(timer, cityList)
    if (current == to) {
      clearInterval(timerId);
      cityList.remove();

    }
    current--;
  }, 1000);
}

printNumbers(3, 0);
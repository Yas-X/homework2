alert('Calculator')
let num1 = prompt('Put Number_1:');
let mathFunct = prompt ('Put math-function:');
let num2 = prompt('Put Number_2:');

while (isNaN(num1) || isNaN(num2) || num1 === '' || num2 === '' || num1 === null || num2 === null) {
    num1 = prompt('Put Number_1:', num1);
    mathFunct = prompt ('Put math-function:', mathFunct);
    num2 = prompt('Put Number_2:', num2);
} 
num1 = +num1;
num2 = +num2;

function calc (num1, num2, mathFunct) {
    if (mathFunct === '+') {
        return (num1 + num2);
    } else if (mathFunct === '-') {
        return (num1 - num2);
    } else if (mathFunct === '*') {
        return (num1 * num2);
    } else if (mathFunct === '/') {
        return (num1 / num2);
    }
}

alert(`${num1} ${mathFunct} ${num2} = ` + calc(num1, num2, mathFunct));
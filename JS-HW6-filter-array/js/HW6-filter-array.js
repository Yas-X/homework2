const array = [true, null, 58, 'text', new Object(), '23', undefined, 47, new Function(), false];
let typeOfData = prompt('Put type of data which you want to exclude from array (string, boolean, null, undefined, symbol, object, function):', 'string');

const filterBy = (newArray, type) => newArray.filter(item => typeof item !== type);

console.log(`Array = ${array}`); 
console.log(`New Array = ${filterBy(array, typeOfData)}`);
alert(`Array = ${array} 
New Array = ${filterBy(array, typeOfData)}`);
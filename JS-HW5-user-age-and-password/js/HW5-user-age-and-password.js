function createNewUser() {

    const newUser = {
        _firstName: '',
        _lastName: '',
        getBirthday: function () {
            birthdayDay = prompt('Put your birthday,please(в формате dd.mm.yyyy):', 'dd.mm.yyyy');
            while (birthdayDay === 'dd.mm.yyyy') {
                birthdayDay = prompt('Put your birthday, CORRECT please(в формате dd.mm.yyyy):', 'dd.mm.yyyy');
            }
            this.birthday = new Date(birthdayDay);
            console.log(this.birthday);
            console.log(this.birthday.getDate());
            console.log(this.birthday.getMonth());
            console.log(this.birthday.getFullYear());
            return this.birthday
        },
        getAge: function () {
            let date = new Date();
            if (date.getMonth() < this.birthday.getMonth()) {
                return date.getFullYear() - this.birthday.getFullYear() - 1
            } else if (date.getDate() < this.birthday.getDate()) {
                return date.getFullYear() - this.birthday.getFullYear() - 1
            } else return date.getFullYear() - this.birthday.getFullYear()
        },
        getPassword: function () {
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear()
        },
        getLogin: function () {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        },
        get firstName() {
            return this._firstName
        },
        set firstName(value1) {
            if (value1 !== null && value1 !== '') {
                this._firstName = value1;
            }
        },
        get lastName() {
            return this._lastName;

        },
        set lastName(value2) {
            if (value2 !== null && value2 !== '') {
                this._lastName = value2;
            }
        },
    };
    newUser.firstName = prompt('What is your name?', 'NoName');
    newUser.lastName = prompt('What is your surname?', 'NoLastName');
    newUser.getBirthday();

    return newUser;
};

newUser = createNewUser();
console.log(`New User LOGIN: ${newUser.getLogin()}`);
console.log(`New User ${newUser.firstName} ${newUser.lastName} is ${newUser.getAge()} years old.`);
console.log(`New User's Password: ${newUser.getPassword()}`);
console.log(newUser);
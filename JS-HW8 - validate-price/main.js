let price = document.getElementsByClassName('priceInput')[0];
//console.log(price);
price.onfocus = function() {
    this.style.borderColor = 'green';
    this.style.borderWidth = '3px';
    };
price.onblur = function() {
    this.style.borderColor = null;
    this.style.borderWidth = '1px';

    if (this.value <= 0) {
        this.style.borderColor = 'red';
        this.style.backgroundColor = 'yellow';
        this.style.borderWidth = '3px';

        if ((document.getElementsByClassName('note').length) == 0) {
            let note = document.createElement('p');
            note.className = 'note';
            note.innerText = 'Please enter correct price';
            document.body.append(note);
        }
        return;
    } else {
        //console.log (document.querySelector('.note'));
        document.querySelector('.note').remove();
    }

    let priceText = this.value;
    let span = document.createElement("span");
    span.innerText = "Текущая цена: " + `${priceText}`+ " ";
    let button = document.createElement("button");
    button.style.borderRadius = '12px';
    button.innerText = 'X';
    document.body.prepend(span, button);
    this.style.backgroundColor = 'green';

    button.onclick = function() {
        span.remove();
        button.remove();
        price.value = null;
    };
};
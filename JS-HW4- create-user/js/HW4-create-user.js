function createNewUser() {

    const newUser = {
        _firstName: 'No Name',
        _lastName: 'No Last Name',

        getLogin: function () {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        },
        get firstName() {
            return this._firstName
        },
        set firstName(value1) {
            if (value1 !== null && value1 !== '') {
                this._firstName = value1;
            }
        },
        get lastName() {
            return this._lastName;

        },
        set lastName(value2) {
            if (value2 !== null && value2 !== '') {
                this._lastName = value2;
            }
        },
    };
    newUser.firstName = prompt('What is your name?', newUser.firstName);
    newUser.lastName = prompt('What is your surname?', newUser.lastName);

    return newUser;
};

newUser = createNewUser();
console.log(`New User LOGIN: ${newUser.getLogin()}`);
console.log(newUser);